package com.synergenhealth.cdms.dataremover.Service;

import com.synergenhealth.cdms.dataremover.domain.*;
import com.synergenhealth.cdms.dataremover.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service @Slf4j
public class DataRemoveService {

    @Autowired
    CreditHistoryRepository creditHistoryRepository;

    @Autowired
    VisitRepository visitRepository;

    @Autowired
    DiagnosisRepository diagnosisRepository;

    @Autowired
    AMDStatusNotesRepository amdStatusNotesRepository;

    @Autowired
    ChargeRepository chargeRepository;

    @Autowired
    CIPTabRepository cipTabRepository;

    @Autowired
    CollectorTabRepository collectorTabRepository;

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    ProviderRepository providerRepository;

    @Autowired
    QATabRepository qaTabRepository;

    @Autowired
    VisitNoteRepository visitNoteRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    WorkQueueRepository workQueueRepository;

    public Boolean process(String officeKey){

        final int licenseKey;
        boolean isDataRemovalSuccess = false;

        try{
            licenseKey = Integer.valueOf(officeKey);
        }catch (NumberFormatException e){
            throw new RuntimeException(e.getMessage());
        }

        log.info("Data deleting process is started for officekey :: {}",licenseKey);

        Optional<Client> clientOptional = clientRepository.findByLicenseCode(officeKey);

        clientOptional.ifPresent(client -> {
                    log.info("client name :: {}",client.getName());
                    workQueueRepository.findAllByClient(client)
                            .ifPresent(workQueueRepository::deleteAll);

                    log.info("Deleting completed all workqueues :: {}",licenseKey);
                });

        clientOptional.orElseThrow(() -> new RuntimeException(String.format("No client found for officekey :: %s", officeKey)));


        List<Visit> visitList = visitRepository.findAllByLicenceKey(licenseKey);

        for(Visit v:visitList){
            Optional<CreditHistory> creditHistory= creditHistoryRepository.findByVisitNumber(v.getVisitNumber());
            Optional<Diagnosis> diagnosis= diagnosisRepository.findByVisitNumber(v.getVisitNumber());

            creditHistory.ifPresent(c ->creditHistoryRepository.delete(creditHistory.get()));
            diagnosis.ifPresent(d ->diagnosisRepository.delete(diagnosis.get()));

        }

        log.info("Deleting completed all credithistory and Diagnosis Codes data :: {}",licenseKey);

        visitRepository.deleteAll(visitList);
        log.info("Deleting completed all visit data :: {}",licenseKey);
        amdStatusNotesRepository.findByLicenseKey(licenseKey).ifPresent(amdStatusNotesRepository::deleteAll);
        log.info("Deleting completed all amdstatusnotes data :: {}",licenseKey);
        chargeRepository.findAllByLicenseKey(licenseKey).ifPresent(chargeRepository::deleteAll);
        log.info("Deleting completed all charge details data :: {}",licenseKey);
        cipTabRepository.findAllByVisitNumber(licenseKey).ifPresent(cipTabRepository::deleteAll);
        log.info("Deleting completed all ciptab data :: {}",licenseKey);
        collectorTabRepository.findAllByVisitNumber(licenseKey).ifPresent(collectorTabRepository::deleteAll);
        log.info("Deleting completed all collectortab data :: {}",licenseKey);
        patientRepository.findAllByLicenceKey(licenseKey).ifPresent(patientRepository::deleteAll);
        log.info("Deleting completed all patient data :: {}",licenseKey);
        providerRepository.findAllByLicenceKey(licenseKey).ifPresent(providerRepository::deleteAll);
        log.info("Deleting completed all provider detail data :: {}",licenseKey);
        qaTabRepository.findAllByLicenceKey(licenseKey).ifPresent(qaTabRepository::deleteAll);
        log.info("Deleting completed all qa-tab data :: {}",licenseKey);
        visitNoteRepository.findAllByLicenceKey(licenseKey).ifPresent(visitNoteRepository::deleteAll);
        log.info("Deleting completed all visit-note data :: {}",licenseKey);
        log.info("Deleted all data :: {}",licenseKey);

        return isDataRemovalSuccess=true;

    }
}
