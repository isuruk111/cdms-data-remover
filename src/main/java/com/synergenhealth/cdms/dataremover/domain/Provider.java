/**
 * Copyright Synergen Health 2018
 * 
 */
package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Provider domain object which represents Providers collection in MongoDB
 * @author Isuruk
 *
 */
@Document(collection = "providers")
@Getter @Setter @ToString
public class Provider implements Serializable {

	private static final long serialVersionUID = 7342631300295676789L;

	@Id
	private String id;

	private String firstname;

	private String lastname;

	private String middlename;

	private int licenceKey;

	private Integer providerUID;

	private String npi;

	private String fullName;

	private String maillingAddress;

	private String physicalAddress;

	private String taxId;

	private String address;

	private String city;

	private String state;

	private String zipCode;

	private String groupCode;

	private String groupName;

	private String profileCode;

	private String financialClassCode;

	private String financialClassDesc;

	private String referrelProvider;

	private String referrelProviderNPI;


}