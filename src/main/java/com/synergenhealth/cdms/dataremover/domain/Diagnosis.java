/**
 * Copyright Synergen Health 2018
 * 
 */
package com.synergenhealth.cdms.dataremover.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Diagnosis domain object which represents Diagnosis collection in MongoDB
 * @author IsuruK
 *
 */
@Document(collection = "diagnosiscodes")
public class Diagnosis implements Serializable {

	private static final long serialVersionUID = 8529648024378944750L;

	@Id
	private String id;

	private int visitNumber;

	private String code;

	private int sequence;

	private int codeSet;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getVisitNumber() {
		return visitNumber;
	}

	public void setVisitNumber(int visitNumber) {
		this.visitNumber = visitNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getCodeSet() {
		return codeSet;
	}

	public void setCodeSet(int codeSet) {
		this.codeSet = codeSet;
	}
}