/*
 * Copyright (c) 2015, SYNERGEN Health. All rights reserved.
 */

package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

/**
 * Notes of visit level.
 *
 * @Author isuruk
 * @Date 5/11/18.
 */

@Document(collection = "visitnotes")
@Getter @Setter @ToString
public class VisitNote implements Serializable {

    @Id
    private String visitNoteId;

    private Integer visitNumber;

    private Integer licenceKey;

    private String note;

    @DBRef
    private QATab qaTab;

    @DBRef
    private CollectorTab collectorTab;

    @DBRef
    private CIPTab cipTab;

    //COLLECTOR, QA, CIP
    private String customTab;

    // SAVED, SUBMITTED;
    private String tabStatus;

}
