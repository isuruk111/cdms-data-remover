package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by isuruk on 5/11/18.
 *
 */

@Document(collection = "qatab")
@Getter @Setter @ToString
public class QATab implements Serializable {

    @Id
    private String id;

    private LocalDate dos;

    private Integer visitNumber;

    private Integer licenceKey;

    private String function;

    private String accuracy;

    private String claimWorkedBy;

    private LocalDate clWorkedDate;

    private String qafollowUpError;

    private String qaCorrectionError;

    private String qaptBillingError;

    private String qaCIP;

    private String qaRecordingError;

    private String qaDocumentation;

    private String qaPostingError;

    private String qaCorrectionStatus;

    private String amdComment;

    private String correctionComment;

    private String cdmsUser;

    private String instanceId;

    private String status;


}
