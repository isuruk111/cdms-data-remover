package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by isuruk on 5/11/18.
 */

@Document(collection = "credithistory")
@Getter @Setter @ToString
public class CreditHistory implements Serializable{

    @Id
    private String id;

    private Integer visitNumber;

    private LocalDate date;

    private String paiedCarrier;

    private String type;

    private BigDecimal amount;


}
