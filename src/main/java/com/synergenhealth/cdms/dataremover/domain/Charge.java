/**
 * Copyright Synergen Health 2018
 */
package com.synergenhealth.cdms.dataremover.domain;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;


/**
 * Charges domain object which represents User collection in MongoDB
 *
 * @author IsuruK on 5/11/2018.
 */
@Document(collection = "charges")
@Getter @Setter @ToString
public class Charge implements Serializable {

    private static final long serialVersionUID = 7400145315001085388L;

    @Id
    private String id;

    private Integer uid;

    private  Integer licenseKey;

    private String cptCode;

    @Indexed
    private Integer visitNumber;

    private LocalDate dateofServiceStartDate;

    private LocalDate dateofServiceEndDate;

    private String modifier;

    private BigDecimal unitCharge;

    private Integer units;

    private BigDecimal billedAmount;

    private BigDecimal writeOffAmount;

    private BigDecimal patientBalance;

    private BigDecimal insuranceBalance;

    private BigDecimal toBeCollected;

    private BigDecimal paidAmount;

    private LocalDate agingDate;

    private String diagnosisCodes;

    private  Integer voidState;

    @DBRef
    private Client client;

    @DBRef
    private Patient patient;

    @DBRef
    private Visit visit;

}