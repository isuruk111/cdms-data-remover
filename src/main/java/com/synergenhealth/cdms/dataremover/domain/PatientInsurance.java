/**
 * Copyright Synergen Health 2018
 */
package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Patient Insurance domain object which represents patientinsurance collection in MongoDB
 *
 * @author IsuruK
 */
@Document(collection = "patientinsurances")
@Getter @Setter @ToString
public class PatientInsurance implements Serializable {

    private static final long serialVersionUID = 1731013434024305408L;

    @Id
    private String id;

    private String carrierName;

    private String insurancenumber;

    private Integer patientInsuranceCoverageUID;

    private LocalDate effectiveStartDate;

    private LocalDate effectiveEndDate;

    private String planType;

    private int coverage;

    private int relationshipToInsured;

    private String subscriberName;

    private LocalDate subscriberDob;

    private String subscriberSsn;

    private String subscriberState;

    private String groupNumber;

    private String groupName;

    @DBRef
    private Patient patient;

}