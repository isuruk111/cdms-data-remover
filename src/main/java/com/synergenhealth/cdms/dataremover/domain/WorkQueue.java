/**
 * Copyright Synergen Health 2018
 */
package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;


/**
 * Work Queue domain object which represents WorkQueues collection in MongoDB
 *
 * @author isuruk
 */
@Document(collection = "workqueues")
@Getter @Setter @ToString
public class WorkQueue implements Serializable {

    private static final long serialVersionUID = -118563275627518189L;

    @Id
    private String workQueueId;

    private String name;

    private String description;

    @Indexed
    private boolean active = true;

    @DBRef
    private Client client;

    private String lastRunBy;

    private String lastRunDate;

    private String archiveBy;

    private String archiveDate;

}
