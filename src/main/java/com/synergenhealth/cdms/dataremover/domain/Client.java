/**
 * Copyright Synergen Health 2018
 */
package com.synergenhealth.cdms.dataremover.domain;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Client domain object which represents Client collection in MongoDB
 *
 *
 * @author Isuruk
 */
@Document(collection = "clients")
@Getter @Setter @ToString
public class Client implements Serializable {

    private static final long serialVersionUID = 7050595938435339131L;

    @Id
    private String id;

    @Indexed(unique = true)
    private String name;

    private String licenseCode;

    @DBRef
    private Set<WorkQueue> workQueues = new HashSet<>();

    private Set<String> headingsSet = new HashSet<>();

    private List<String> additionalEditFields = new ArrayList<>();

    private String deniedWorkqueueId;

    private String pendingCredentialingWQId;

    private String cipWQId;

    private String patientBalanceWQId;

    private String payerHoldsWQId;

    private String billingMethod;



}