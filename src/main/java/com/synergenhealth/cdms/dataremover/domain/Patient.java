/**
 * Copyright Synergen Health 2018
 */
package com.synergenhealth.cdms.dataremover.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Patient domain object which represents patients collection in MongoDB
 *
 * @author IsuruK
 */
@Document(collection = "patients")
@Getter @Setter @ToString
public class Patient implements Serializable {

    private static final long serialVersionUID = -6510042531852684063L;

    @Id
    private String patientId;

    private String chartNumber;

    private int licenceKey;

    private Integer patientNumber;

    private String firstName;

    private String lastName;

    private String middleName;

    private String fullName;

    private LocalDate dateOfBirth;

    private String gender;

    private String ssn;

    private int relationship;

    private String financialClass;

    private LocalDate amdCreatedAt;

    private String address;

    private String zipCode;

    private String city;

    private String areaCode;

    private String state;

    private String phone;

    private String email;

}