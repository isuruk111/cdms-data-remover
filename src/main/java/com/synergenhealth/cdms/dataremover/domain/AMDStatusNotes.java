package com.synergenhealth.cdms.dataremover.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * AMD Status Notes domain object which represents AMD Status Notes collection in MongoDB
 * <p>
 * Created by IsuruK on 5/11/2018.
 */
@Document(collection = "amdstatusnotes")
@CompoundIndexes({
        @CompoundIndex(name="licenceKey_visitNo", def = "{'licenseKey':1,'visitNumber':1}"),
        @CompoundIndex(name = "licenceKey_chartNo", def = "{'licenseKey':1,'chartNumber':1}")
})
@Getter @Setter @ToString
public class AMDStatusNotes implements Serializable {

    @Id
    private String id;

    private Integer licenseKey;
    private Integer patientFId;
    private Integer chartNumber;
    private Integer userFileInstanceUId;
    private LocalDateTime dos;
    private String balanceType;
    private Integer visitNumber;
    private String status;
    private String followUp;
    private String comments;
    private String followUpDate;
    private String claimType;
    private String workType;
    private String secondaryStatus;
    private String fuStatus;
    private String cdmsUser;
    private LocalDateTime amdCreatedAt;
    private String amdCreatedBy;
    private LocalDateTime amdChangedAt;
    private String amdChangedBy;

}
