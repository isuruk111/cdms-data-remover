package com.synergenhealth.cdms.dataremover.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by isuruk on 5/11/18.
 *
 * Following implementation will be changing in future after standarization
 */

@Document(collection = "collectortab")
@Setter @Getter @ToString
public class CollectorTab implements Serializable {

    @Id
    private String id;

    private LocalDate dos;

    private Integer visitNumber;

    private Integer licenceKey;

    private String balanceType;

    private String claimType;

    private String workType;

    private String status;

    private String fuStatus;

    private String secondaryStatus;

    private LocalDate followUpDate;

    private String internalComment;

    private String externalComment;

    private String instanceId;

    private String cdmsUser;

}
