package com.synergenhealth.cdms.dataremover.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * Created by IsuruK on 5/11/2018.
 */
@Document(collection = "ciptab")
@Getter @Setter @ToString
public class CIPTab implements Serializable{

    @Id
    private String id;

    private LocalDate dos;
    private Integer visitNumber;
    private Integer licenceKey;
    private String cipType;
    private String requiredInformation;
    private String deniedInsurance;
    private String deniedSubsInsId;
    private String cipComments;
    private String cipStatus;
    private LocalDate cipClosedDate;
    private String reOpenedReason;
    private String cdmsUser;
    private String instanceId;
    private String status;

}
