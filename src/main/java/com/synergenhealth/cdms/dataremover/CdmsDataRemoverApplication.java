package com.synergenhealth.cdms.dataremover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdmsDataRemoverApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdmsDataRemoverApplication.class, args);
	}
}
