package com.synergenhealth.cdms.dataremover.webcontroller;


import com.synergenhealth.cdms.dataremover.Service.DataRemoveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController @Slf4j
public class MainController {

    @Autowired
    DataRemoveService dataRemoveService;

    @PutMapping("/delete")
    public ResponseEntity<String> deleteOfficeKeydata(@RequestParam(name="officekey", required = true, defaultValue = "11") String officeKeyValue){

        log.info("officekey value :: {}",officeKeyValue);
        if (dataRemoveService.process(officeKeyValue)){
            return new ResponseEntity<String>("Successfully Deleted All data of "+officeKeyValue,HttpStatus.OK);
        }else {
            return new ResponseEntity<String>("Issue happened during the process of deletion "+officeKeyValue,HttpStatus.OK);
        }

    }
}
