package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.CIPTab;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CIPTabRepository extends MongoRepository<CIPTab,String> {

    Optional<List<CIPTab>> findAllByVisitNumber(Integer visitNumber);
}
