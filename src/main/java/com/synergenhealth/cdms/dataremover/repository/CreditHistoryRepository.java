package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.CreditHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CreditHistoryRepository extends MongoRepository<CreditHistory, String> {

    Optional<CreditHistory> findByVisitNumber(Integer visitNumber);
}
