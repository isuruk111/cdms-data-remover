package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.AMDStatusNotes;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface AMDStatusNotesRepository extends MongoRepository<AMDStatusNotes, String> {

    Optional<List<AMDStatusNotes>> findByLicenseKey(Integer licenseKey);
}
