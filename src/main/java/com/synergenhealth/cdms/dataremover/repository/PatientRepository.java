package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends MongoRepository<Patient, String> {

    Optional<List<Patient>> findAllByLicenceKey(int licenceKey);
}
