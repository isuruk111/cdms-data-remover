package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.Diagnosis;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DiagnosisRepository extends MongoRepository<Diagnosis, String> {

    Optional<Diagnosis> findByVisitNumber(Integer visitNumber);
}
