package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.Charge;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ChargeRepository extends MongoRepository<Charge, String> {

    Optional<List<Charge>> findAllByLicenseKey(Integer licenseKey);
}
