package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.CollectorTab;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CollectorTabRepository extends MongoRepository<CollectorTab, String> {

    Optional<List<CollectorTab>> findAllByVisitNumber(Integer visitNumber);
}
