package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.QATab;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface QATabRepository extends MongoRepository<QATab, String> {

    Optional<List<QATab>> findAllByLicenceKey(Integer licenceKey);
}
