package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.Provider;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ProviderRepository extends MongoRepository<Provider,String> {

    Optional<List<Provider>> findAllByLicenceKey(int licenceKey);
}
