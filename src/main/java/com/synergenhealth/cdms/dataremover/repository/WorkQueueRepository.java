package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.Client;
import com.synergenhealth.cdms.dataremover.domain.WorkQueue;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface WorkQueueRepository extends MongoRepository<WorkQueue, String> {

    Optional<List<WorkQueue>> findAllByClient(Client client);
}
