package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.VisitNote;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface VisitNoteRepository extends MongoRepository<VisitNote, String> {

    Optional<List<VisitNote>> findAllByLicenceKey(Integer licenceKey);
}
