package com.synergenhealth.cdms.dataremover.repository;

import com.synergenhealth.cdms.dataremover.domain.PatientInsurance;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PatientInsuranceRepository extends MongoRepository<PatientInsurance, String> {
}
